# Notes


# Mailing

## Inbucket
Inbucket is an email testing service; it will accept messages for any email address and make them available via web, REST and POP3 interfaces.

Run on local machine:
~~~bash
docker run -d --name inbucket -p 9000:9000 -p 25:2500 -p 110:1100 -e INBUCKET_SMTP_DOMAIN="mail.com" inbucket/inbucket                           
~~~

GitHub: https://github.com/inbucket/inbucket

